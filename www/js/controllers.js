angular.module('starter.controllers', [])

.controller('connectionController', function($scope, $rootScope, $timeout, BluetoothService) {
	function init() {
		$scope.loading = true;
		$scope.error = false;
		$scope.message = "Disconnected";
		$scope.connected = false;
		$scope.rpm = 0;
		$scope.load = 0;
		$scope.speed = 0;
		$scope.iat = 0;
	}

	init();
	$scope.message = "Connecting..";

	$scope.retry = function () {
		init();
		$scope.message = "Connecting..";
		BluetoothService.initialize();
	}

	$scope.connection = function () {
		if ($scope.loading) {
			return;
		}
		if ($scope.connected) {
			BluetoothService.disconnect();
		} else {
			$scope.retry();
		}
	};

	$rootScope.$on('data', function(event, data) {
		$scope.$apply(function () {
			$scope.data = $scope.data + JSON.stringify(data);
			if (data.name === 'rpm') {
				$scope.chartConfig.series[0].data = [$scope.rpm];
				$scope.chartConfig.series[1].data = [$scope.fuelEff()];
				$scope.rpm = data.value;
			} else if (data.name === 'vss') {
				$scope.chartConfig.series[1].data = [$scope.fuelEff()];
				$scope.speed = data.value;
			} else if (data.name === 'map') {
				$scope.chartConfig.series[1].data = [$scope.fuelEff()];
				$scope.load = data.value;
			} else if (data.name === 'iat') {
				$scope.chartConfig.series[1].data = [$scope.fuelEff()];
				$scope.iat = data.value;
			}
		});
	});

	$scope.fuelEff = function () {
		//IMAP = RPM * MAP / IAT MAF = (IMAP/120)*(VE/100)*(ED)*(MM)/(R
		//MPG = (14.7 * 6.17 * 454 * VSS * 0.621371) / (3600 * MAF / 100) MPG
		//= 710.7 * VSS / MAF
		//https://github.com/oesmith/obdgpslogger/blob/master/doc/mpg-calculation
		var iat;
		if ($scope.iat < 0 || $scope.iat == 0) {
			iat = 273 - 40;
		} else {
			iat = ($scope.iat - 40 + 273); //Convert to K
		}

		var ve;
		if ($scope.rpm < 1500) {
			ve = 0.4;
		} else if ($scope.rpm < 2000) {
			ve = 0.5;
		} else if ($scope.rpm < 2500) {
			ve = 0.7;
		} else {
			ve = 0.9;
		}

		var rpm = $scope.rpm/4;

		var imap = (rpm * $scope.load) / iat;
		var speed = $scope.speed;

		var maf = ((imap/120) * (ve * 100) * (28.97 * 1))/8.314;
		//var maf = (map/100) * (rpm/120) * (1000/1000) * (1/(iat + 273)) * (1/0.082057) * 28.75 * ve;
		//mpg to kmpl: http://www.mpgtokpl.net/calculator/mpg-miles-gallon-us-kpl-kilometers-liter-calculator
		var eff =  710 * (speed/maf) * 0.34;
		eff = parseFloat(eff.toFixed(2));
		//Safeguard till calculations can be improved
		//if (eff > 30) {
			//eff = 29.98;
		//}
		return _.isNaN(eff) ? 0 : eff;
	}

	$rootScope.$on('connected', function(event, device) {
		$scope.$apply(function () {
			$scope.message = "Connected";
			$scope.connected = true;
		});
		$timeout(function () {
			$scope.loading = false;
		}, 3000);
	});

	$rootScope.$on('disconnected', function(event, err) {
		$scope.$apply(function () {
			init();
			$scope.loading = false;
		});
	});

	$rootScope.$on('notConnected', function(event, err) {
		console.log("Error", JSON.stringify(err));
		$scope.$apply(function () {
			$scope.error = true;
			$scope.message = "Is the OBD connected?";
			$scope.loading = false;
			$scope.connected = false;
		});
	});

	$rootScope.$on('notPaired', function() {
		$scope.$apply(function () {
			$scope.error = true;
			$scope.message = "Is the OBD paired?";
			$scope.loading = false;
			$scope.connected = false;
		});
	});

	$rootScope.$on('error', function() {
		$scope.$apply(function () {
			$scope.error = true;
			$scope.message = "Sorry, something went wrong";
			$scope.loading = false;
			$scope.connected = false;
		});
	});

	$rootScope.$on('notEnabled', function() {
		$scope.$apply(function () {
			$scope.error = true;
			$scope.message = "Please enable bluetooth";
			$scope.loading = false;
			$scope.connected = false;
		});
	});

	$scope.chartConfig = {
		options: {
			chart: {
				type: 'solidgauge'
			},

			pane: {
				center: ['50%', '50%'],
				size: '100%',
				startAngle: -180,
				endAngle: 180,
				background: {
					backgroundColor:'#EEE',
					innerRadius: '60%',
					outerRadius: '100%',
					shape: 'arc',
					borderColor: 'transparent'
				}
			}
		},

		credits: {
			enabled: false
		},

		title: {
			text: null
		},

		yAxis: [{
			min: 0,
			max: 4000,
			title: {
				enabled: false
			},      
			stops: [
				[0.1, '#55BF3B'], // green
				[0.2, '#DDDF0D'], // yellow
				[0.7, '#DF5353'] // red
			],
			lineWidth: 0,
			minorTickInterval: null,
			tickInterval: 100,
			tickPixelInterval: 400,
			tickWidth: 0,
	 		gridLineWidth: 0,
			gridLineColor: 'transparent',
			labels: {
				enabled: false
			}
		},
		{
			min: 0,
			max: 30,
			opposite: true,
			title: {
				enabled: false
			},      
			stops: [
				[0.2, '#DF5353'], // red
				[0.4, '#DDDF0D'], // yellow
				[0.7, '#55BF3B'] // green
			],
			lineWidth: 0,
			minorTickInterval: null,
			tickInterval: 1,
			tickPixelInterval: 400,
			tickWidth: 0,
	 		gridLineWidth: 0,
			gridLineColor: 'transparent',
			labels: {
				enabled: false
			}
		}],

		series: [
			{
				name: 'rpm',
				data: [0],
				dataLabels: {
					format: '<div style="text-align:center"><span style="font-size:15px;color:black">{y}</span><br><span>RPM</span></div>',
					y: -45,
					borderWidth: 0,
					useHTML: true
				},
				innerRadius: 97,
				zIndex: 1,
				yAxis: 0
			},
			{
				name: 'fuelEff',
				data: [0],
				dataLabels: {
					format: '<div style="text-align:center"><span style="font-size:35px;color:black">{y}</span><br><span>km/lit</span></div>',
					y: -5,
					borderWidth: 0,
					useHTML: true
				},
				innerRadius: 60,
				yAxis: 1
			}
		]
	};
});
