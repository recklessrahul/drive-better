angular.module('starter.directives', [])

.directive('gauge', function($compile, $document) {
	return {
		restrict: 'E',
		scope: {
			chartConfig: '='
		},
		link: function (scope, element, attrs) {
			var template = "<highchart id='gauge' config='chartConfig' class='span10'></highchart>";
			$compile(template)(scope);
			$document.ready(function() {
				element.replaceWith(template);
			});
		}
	};
});
