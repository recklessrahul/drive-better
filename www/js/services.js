var intervalId;
angular.module('starter.services', [])
.factory('BluetoothService', function($rootScope, ObdService) {
	var connect = function (device) {
		bluetoothSerial.connect(device.id, function () {
			ObdService.init();
		}, function (err) {
			$rootScope.$broadcast('notConnected', err);
		});
	};

	var listPorts = function () {
		bluetoothSerial.list(function(results) {
			var device = _.find(results, function (device) {
				return device.name.match(/rahul/i);
			});

			if (device) {
				connect(device);
			} else {
				$rootScope.$broadcast('notPaired');
			}
		}, function(error) {
			$rootScope.$broadcast('error', error);
		});
	}

	var notEnabled = function() {
		$rootScope.$broadcast('notEnabled');
	}

	return {
		initialize: function() {

			function init() {
				bluetoothSerial.isEnabled(listPorts, notEnabled);
			}
			if (intervalId) {
				window.clearInterval(intervalId);
			}
			bluetoothSerial.disconnect(init, init);
		},
		disconnect: function() {
			if (intervalId) {
				window.clearInterval(intervalId);
			}
			bluetoothSerial.unsubscribe('>');
			bluetoothSerial.disconnect(function () {
				$rootScope.$broadcast('disconnected');
			}, function () {
				$rootScope.$broadcast('error');
			});
		}
	};
})
.factory('ObdService', function($rootScope, PIDS) {
	var queue = [];
	var startWrite = function () {
    intervalId = window.setInterval(function writing() {
			queue.push('010C');
			queue.push('010B');
			queue.push('010D');
			queue.push('010F');
			if (queue.length > 0) {
				bluetoothSerial.write(queue.shift() + '\r', function () {
				}, function (err) {
					console.log("Err", err);
				});
			}
		}, 300);
	}

	function parseOBDCommand(hexString) {
		var reply,
		byteNumber,
		valueArray; //New object
		hexString = hexString.trim();

		reply = {};
		if (hexString === "NO DATA" || hexString === "OK" || hexString === "?") { //No data or OK is the response.
			reply.value = hexString;
			return reply;
		}

		hexString = hexString.replace(/ /g, ''); //Whitespace trimming //Probably not needed anymore?
		valueArray = [];

		for (byteNumber = 0; byteNumber < hexString.length; byteNumber += 2) {
			valueArray.push(hexString.substr(byteNumber, 2));
		}

		if (valueArray[0] === "41") {
			reply.mode = valueArray[0];
			reply.pid = valueArray[1];
			var selectedPID = _.find(PIDS, function (pid) {
				return pid.pid === reply.pid
			});
			if(selectedPID) {
				var numberOfBytes = selectedPID.bytes;
				reply.name = selectedPID.name;
				switch (numberOfBytes)
				{
					case 1:
						reply.value = selectedPID.convertToUseful(valueArray[2]);
					break;
					case 2:
						reply.value = selectedPID.convertToUseful(valueArray[2], valueArray[3]);
					break;
					case 4:
						reply.value = selectedPID.convertToUseful(valueArray[2], valueArray[3], valueArray[4], valueArray[5]);
					break;
					case 8:
						reply.value = selectedPID.convertToUseful(valueArray[2], valueArray[3], valueArray[4], valueArray[5], valueArray[6], valueArray[7], valueArray[8], valueArray[9]);
					break;
				}
			}
		} else if (valueArray[0] === "43") {
			reply.mode = valueArray[0];
			var selectedPID = _.find(PIDS, function (pid) {
				return pid.pid === reply.pid
			});
			if(selectedPID.mode == "03") {
				reply.name = selectedPID.name;
				reply.value = selectedPID.convertToUseful(valueArray[1], valueArray[2], valueArray[3], valueArray[4], valueArray[5], valueArray[6]);
			}
		}
		return reply;
	}

	var init = function () {
		queue.push('ATZ');
		queue.push('ATL0');
		queue.push('ATS0');
		queue.push('ATH0');
		queue.push('ATE0');
		queue.push('ATAT2');
		queue.push('ATSP0');
		$rootScope.$broadcast('connected', device);

		bluetoothSerial.subscribe('>', function (data) {
			data = data.replace(/>/, '');
			var response = parseOBDCommand(data)
			$rootScope.$broadcast('data', response);
		}, function (err) {
			console.log('Error', err);
		});

		startWrite();
	};

	return {
		init: init
	};
});
